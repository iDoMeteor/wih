Notifications = new Meteor.Collection('notifications');

/*
 * Permissions for Notification collection
 */
Notifications.allow({
    insert: function () {
        // Ignoring anonymous users
        if (Meteor.user()) return true;
    },
    update: function () { // For marking as read
       return (isOwner(Meteor.userId(), this));
    },
    remove: false,
});

/*
 * Notification methods
 * 
 * TODO:
 *  Should these be in /server ?
 */
Meteor.methods ({

    /*
     * Create notification
     *
     * TODO:
     *  Should we add an optional parent id?
     *  If so, get it's info w/findOne
     */
    notificationCreate: function (object, action, anonymous) {

        // Do not create notifications for user's own actions
        if (
            (!object.userId) 
            || (this.userId == object.userId)
        ) {
           return false;
        }
        if (!!anonymous) {
            // TODO: 
            //  This should only be true for comments
            //  which probably need their own method
            username = "Anonymous registered user";
        } else {
            username = Meteor.displayName(Meteor.user());
        }

        // Populate notification object
        var notification = {
            stamp: new Date().getTime(),
            objectId: object._id,
            ownerId: object.userId,
            userId: Meteor.userId(),
            username: username,
            action: action,
            ack: false,
        };
        var notificationId = Notifications.insert(notification);

        return notificationId;

    },

    /*
     * Mark notification as read
     */
    notificationRead: function (notificationId) {

        Notifications.update (notificationId, {
            $set: {ack: true}
        });
    }

});
