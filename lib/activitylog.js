ActivityLog = new Meteor.Collection('activitylog');

/*
 * Permissions for activity log collection
 *
 */
ActivityLog.allow({
    insert: function () {
        // Ignoring anonymous users
        return !! Meteor.user();
    },
    update:  false,
    remove: false,
});

/*
 * ActivityLog methods
 * 
 */
Meteor.methods ({

    /*
     * Create activity log
     *
     */
    activitylogCreate: function (o, action) {

        // Populate activitylog object
        if (!o.postId) {
            var activitylog = {
                stamp: new Date().getTime(),
                objectId: o._id,
                userId: Meteor.userId(),
                action: action,
            };
        } else {
            var activitylog = {
                stamp: new Date().getTime(),
                objectId: o._id,
                postId: o.postId, // Only matters for comments
                userId: Meteor.userId(),
                action: action,
            };
        }
        // Insert it
        var activitylogId = ActivityLog.insert(activitylog);
        // Return it
        return activitylogId;

    }

});
