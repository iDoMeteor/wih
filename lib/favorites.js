Favorites = new Meteor.Collection('favorites');

/*
 * Permissions for Notifcation collection
 */
Favorites.allow({
    insert: function () {
        // Might want to ignore non-logged in users
        // if (Meteor.user()) return true;
        return !! Meteor.user();
    },
    update: function () {
       // Not used yet
       return (isOwner(Meteor.userId(), this));
    },
    remove: function () {
       // One of the few actual deletions we allow
       return (isOwner(Meteor.userId(), this));
    }
});

/*
 * Favorite methods
 * 
 * TODO:
 *  Should these be in /server ?
 */
Meteor.methods ({

    /*
     * Create favorite
     */
    favoriteAdd: function (o) {

        // Populate favorite object
        var favorite = {
            stamp: new Date().getTime(),
            objectId: o._id,
            userId: this.userId,
        };
        
        // TODO:
        // Check for dupes or upsert this
        var favoriteId = Favorites.insert(favorite);
        
        // Create notification upon success
        if (favoriteId) {
            
            // Grab object owner's id to create notification
            post = Posts.findOne({_id: o._id});

            // Call notification
            Meteor.call ('notificationCreate', 
                    post, 
                    Strings.favoriteAdded, 
                    false, 
                    function (error) {
                        if (error) {
                            // Notification failure does throw errors atm
                            // errorThrow(error.reason);
                        } 
                    }
            );
            return favoriteId;
        } else {
            return false;
        }

    },

    /*
     * Remove favorite
     *
     * This is now only for posts
     *
     */
    favoriteRemove: function (o) {

        // Remove this object from current user's favorites
        Favorites.remove({objectId: o._id, userId: this.userId});

        // Grab object owner's id to create notification
        post = Posts.findOne({_id: o._id});

        // Create notification
        Meteor.call ('notificationCreate', 
                post, 
                Strings.favoriteRemoved, 
                false, 
                function (error) {
                    if (error) {
                        // Notification failure does throw errors atm
                        // errorThrow(error.reason);
                    } 
                }
        );

    },

    /*
     * Determine whether or not object is a favorite
     */
    isFavorite: function (id) {

        var favorite = {
            objectId: id,
            userId: this.userId,
        };
        var result = Favorites.findOne (favorite);
        return result;
        
    }

});




// create favorite methods
// complete favorites w/o notifications
// flags are just the opposite of favorites
// once both are done, create notifications for each
// then notifications for post & comments hates
// the for comments themselves (maybe between the hate lol




/*
isFavorite



                } else {
                    // Create notification
                    Notifications.create(this._id, 'unfavorited');
                    // Swap glyphs
                    $(event.currentTarget).innerHTML(
                        '<span title="Favorite this" class="glyphicon glyphicon-star-empty"></span>'
                    );
                }

                } else {
                    // Create notification
                    Notifications.create(this._id, 'favorited');
                    // Swap glyphs
                    $(event.currentTarget).innerHTML(
                        '<span title="Favorite this" class="glyphicon glyphicon-star"></span>'
                    );
                }

*/
