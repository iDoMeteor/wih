/*
 * Site string configuration
 */
Strings = new Meteor.Collection("strings");

Strings.chatLimit           = 10;
Strings.chatPrompt          = "Spit & h1t enta";
Strings.commentAdded        = "commented on";
Strings.commentFlagged      = "flagged";
Strings.commentHated        = "hated on";
Strings.contactName         = "y0 nAme";
Strings.contactMail         = "sPamdr3sss";
Strings.contactMesg         = "smAsH enta 2 p0p oFF";
Strings.contactRcpt         = "tanx 4 ur c0mm3ntz!";
Strings.favoriteAdded       = "favorited";
Strings.favoriteLimit       = 10;
Strings.favoriteRemoved     = "unfavorited";
Strings.logCommentFlag      = "flagged";
Strings.logCommentHate      = "hated on";
Strings.logCommentNew       = "ragged on";
Strings.logPostFavorite     = "favorited";
Strings.logPostFlag         = "flagged";
Strings.logPostHate         = "hated on";
Strings.logPostNew          = "made beef in";
Strings.logPostUnfavorite   = "unfavorited";
Strings.noteReason          = "Smack return to submit.  In fact, do it twice cuz there's a fucking bug.  Deal with it.";
Strings.noteTarget          = "This will be your tweet..when that feature is available >:o";
Strings.notificationLimit   = 10;
Strings.postAction          = "hates";
Strings.postConjunction     = "because";
Strings.postFavorited       = "favorited";
Strings.postFlagged         = "flagged";
Strings.postHated           = "hated on";
Strings.postUnfavorited     = "unfavorited";
Strings.promptAnonymous     = "Post anonymously?";
Strings.promptReason        = "because:";
Strings.promptTarget        = "What I hate is:";
Strings.siteTitle           = "What I Hate";
