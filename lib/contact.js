ContactLog = new Meteor.Collection('contactlog');

/*
 * Permissions for activity log collection
 *
 */
ContactLog.allow({
    insert: function () {
        // Ignoring anonymous users
        return true;
    },
    update:  false,
    remove: false,
});
