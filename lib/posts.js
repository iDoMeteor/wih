/*
 * Posts provides:
 *  stamp
 *  user
 *  target
 *  reason
 *  hates
 *
 */
Posts = new Meteor.Collection("posts");
HateLog = new Meteor.Collection("hatelog");
FlagLog = new Meteor.Collection("flaglog");
