/*
 * Function to check if user is logged in
 */
requireLogin = function() {
    if (! Meteor.user()) {
        if (Meteor.loggingIn()) {
            this.render('loading');
        } else {
            this.render('accessDenied');
        }
        this.stop();
    }
}

/*
 * Check for ownership
 */
isOwner = function (userId, object) {
    return object && object.userId == userId;
}

/*
 * Check for admin
 *
 * TODO:
 *  Make functional
 */
isAdmin = function (userId, object) {
    return false;
}

Meteor.displayName = function(user) {
  if (!user)
    return '';
  if (user.profile && user.profile.name)
    return user.profile.name;
  if (user.username)
    return user.username;
  if (user.emails && user.emails[0] && user.emails[0].address)
    return user.emails[0].address;
  return '';
};
