Flags = new Meteor.Collection('flags');

/*
 * Permissions for Notifcation collection
 */
Flags.allow({
    insert: function () {
        // Must be logged in to flag
        return !! Meteor.user();
    },
    update: function () {
       // Not used yet
       return false;
    },
    remove: function () {
       // Not used yet
       return false;
    }
});

/*
 * Flag methods
 */
Meteor.methods ({

    /*
     * Create flag
     */
    flagAdd: function (id) {

        // Populate flag object
        // Use IP address for non-registered users?
        //
        // Maybe nest userid &/or ip in post object?
        //   or add each separately..
        //
        var flag = {
            objectId: id,
            userId: this.userId,
        };
        var flagId = Flags.insert(flag);
        return flagId;

    },

    /*
     * Remove flag
     */
    flagRemove: function (id) {

        Flags.remove({objectId: id, userId: this.userId});

    },

    /*
     * Determine whether or not object is a flag
     */
    isFlagged: function (id) {

        var flag = {
            objectId: id,
            userId: this.userId,
        };
        var result = Flags.findOne (flag);
        return result;
        
    }

});




// create flag methods
// complete flags w/o notifications
// flags are just the opposite of flags
// once both are done, create notifications for each
// then notifications for post & comments hates
// the for comments themselves (maybe between the hate lol




/*
isFlag



                } else {
                    // Create notification
                    Notifications.create(this._id, 'unflagd');
                    // Swap glyphs
                    $(event.currentTarget).innerHTML(
                        '<span title="Flag this" class="glyphicon glyphicon-star-empty"></span>'
                    );
                }

                } else {
                    // Create notification
                    Notifications.create(this._id, 'flagd');
                    // Swap glyphs
                    $(event.currentTarget).innerHTML(
                        '<span title="Flag this" class="glyphicon glyphicon-star"></span>'
                    );
                }

*/
