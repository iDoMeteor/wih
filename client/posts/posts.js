/*
 * Post list triggers
 */
Template.postList.events({
    
    'click #posts-load-more': function (event) {
        Router.go(this.nextPath);
    },

});
