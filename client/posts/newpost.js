Template.postNew.created = function() {

    // Template variables
    Template.postNew.promptTarget = function () {
        return Strings.promptTarget;
    };
    Template.postNew.promptReason = function () {
        return Strings.promptReason;
    };
    Template.postNew.promptAnonymous = function () {
        return Strings.promptAnonymous;
    };
    Template.postNew.noteTarget = function () {
        return Strings.noteTarget;
    };
    Template.postNew.noteReason = function () {
        return Strings.noteReason;
    };

}

/*
 * Edit in place by converting values to inputs
 */

/*
 * New post triggers
 */
Template.postNew.events({

    /*
     * Contract container when clicking minus sign div
     */
    'click #new-post-rollover-minus': function (event) {
        $("#new-post-rollover-minus").slideUp("", function() {
            $("#new-post-rollover-plus").show();
        });
        $("#new-post-form").slideUp("slow");
    },

    /*
     * Expand container when clicking plus sign div
     */
    'click #new-post-rollover-plus': function (event) {
        $("#new-post-rollover-plus").hide();
        $("#new-post-rollover-minus").slideDown("slow");
        $("#new-post-form").slideDown("slow");
        $("#new-post-target").focus();
    },

    /*
     * Close new post form when Esc key is pressed
     */
    'keyup #new-post-form': function (e) {
        if (e.which === 27) {
            $("#new-post-rollover-minus").slideUp("", function() {
                $("#new-post-rollover-plus").show();
            });
            $("#new-post-form").slideUp("slow");
        }
    },

    /*
     * Submit form in place when enter is pressed in input field
     */
    'keypress #new-post-target': function (e) {
        if (e.which === 13) {
            $("#new-post-form").submit(function(e){return false});
        }
    },

    /*
     * Submit form in place when enter is pressed in text area
    */
    'keypress #new-post-reason': function (e) {
        if (e.which == 13) {
            $("#new-post-target").focus();
// Move submission functions directly into here
            $("#new-post-form").submit(function(e){return false});
        }
    },

    /*
     * New post submission
     * Retard default action
     *   Check if can be removed by assigning post action to #
     * Populate client side values
     * Trigger server method
     *   Send post to postNew method which returns error
     * Clear form fields
     * Contract new post containers and replace with expansion container
     */
    'submit': function(e) {
        e.preventDefault();
        if (!Meteor.user()) {
            errorThrow("Gotta be logged in, sucka");
            return false;
        }
        var post = {
            stamp: new Date().getTime(),
            username: Meteor.user().username,
            target: $(e.target).find('[name=target]').val().trim(),
            reason: $(e.target).find('[name=reason]').val().trim(),
            anonymous: $('input[name=anonymous]:checked').val(),
            comments: 0,
            flags: 0,
            haters: 0,
        };
        Meteor.call ('postNew', post, function (error) {
            if (error) {
                errorThrow(error.reason);
                if (error.error === 302) {
                    // dupe, redirect to post
                }
            } else {
                // Should be only on success
                $("#new-post-form").find("input[type=text], textarea").val("");
                // Maybe delay this briefly
                $("#new-post-rollover-minus").slideUp("", function() {
                    $("#new-post-rollover-plus").show();
                });
                $("#new-post-form").slideUp("slow");
            }
        });
        // Prevent page reload
        return false;
    }

});
