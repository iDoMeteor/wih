Template.post.created = function () {
   // Template Variables
    Template.post.action = function () {
        return Strings.postAction;
    };
    Template.post.conjunction = function () {
        return Strings.postConjunction;
    };
};

/*
 * Post template helpers
*/
Template.post.helpers({
    date: function() {
        date = moment(this.stamp).fromNow();
        return date;
    },
    isFavorite: function() {
        var exists = Favorites.findOne({
            objectId: this._id,
            userId: Meteor.userId(),
        });
        if (exists) {
            return true;
        } else {
            return false;
        }
    },
    ownPost: function() {
        // This should use IDs, DUH
        if (!Meteor.user()) return false;
        p = this.username;
        u = Meteor.user().username;
        if (p == u) {
            return true;
        } else {
            return false;
        }
    },
});

/*
 * Edit in place by converting values to inputs
 */

/*
 * Post triggers
 */
Template.post.events({
    
    /*
     * Load single post
    'click .post-link': function (e) {
        Router.go('postPage', {_id: this._id});
    },
     */
    /*
     * Favorite post
     */
    'click .post-favorite': function (e) {
        // Can't favorite something if you're not a user!
        if (!Meteor.user()) {
            errorThrow("Gotta be logged in, sucka");
            return false;
        }
        // Add favorite to database
        Meteor.call ('favoriteAdd', this, function (error) {
            if (error) {
                errorThrow(error.reason);
            } 
        });

    },
    'click .post-favorited': function (e) {
        // Remove favorite to database
        Meteor.call ('favoriteRemove', this, function (error) {
            if (error) {
                errorThrow(error.reason);
            } 
        });
    },
    /*
     * Edit post
     */
    'click .post-edit': function (e) {
        // Check for admin
    },
    /*
     * Show and hide post flagger
     */
    'mouseenter .post-flag-container': function (e) {
        $(e.currentTarget).children(".post-meta").children(".post-flag").show();
    },
    'mouseleave .post-flag-container': function (e) {
        $(e.currentTarget).children(".post-meta").children(".post-flag").hide();
    },
    /*
     * Flagging post
     * Insert into hidden collection
     * Hidden for all or just for user
     * Need post id
     *
     */
    'click .post-flag': function (e) {
        Meteor.call ('flagPost', this._id, function (error) {
            if (error) {
                errorThrow(error.reason);
            }
        });
    },
    /*
     * Expand & contract comment triggers
     */
    'click .comment-contractor': function (e) {
        // Set this post back to default state
        $(e.currentTarget).hide();
        $(e.currentTarget).siblings(".comment-expander").show();
        $(e.currentTarget).parent().parent().children(".post-comments").children(".comments-container").slideUp("slow");
    },
    'click .comment-expander': function (e) {
        // Set all other posts back to default state
        $(".comments-container").slideUp("slow");
        $(".comment-contractor").hide();
        $(".comment-expander").show();
        // Toggle the toggler & show the comments
        $(e.currentTarget).hide();
        $(e.currentTarget).siblings(".comment-contractor").show();
        $(e.currentTarget).parent().parent().children(".post-comments").children(".comments-container").slideDown("slow");

        /*
         * Load comments on first expansion
         */
    },
    /*
     * Hate on it
     */
    'click .hate-on-it': function (e) {
        Meteor.call ('hatePost', this._id, function (error) {
            if (error) {
                errorThrow(error.reason);
            }
        });
    },

});

/*
 * Run each time a post template is rendered
 *
 * TODO:
 *      Probably a good place to count post views
 *      Would also be useful for ads!
 */
Template.post.rendered = function () {
    

};
