// This needs to be more selective
commentSub = Meteor.subscribe('comments');
if (!commentSub.ready()) {
    // Waiting
}

Template.newComment.promptComment = function () {
    return "Why u hatin':";
};
Template.newComment.noteComment = function () {
    return "This will be tweeted..sooner or later >:o";
};
Template.newComment.promptCommentAnonymous = function () {
    return "Comment anonymously?";
};

/*
 * Comment template helpers
 */
Template.comment.helpers({
    ownComment: function() {
        if (!Meteor.user()) return false;
        c = this.username;
        u = Meteor.user().username;
        if (c == u) {
            return true;
        } else {
            return false;
        }
    },
    date: function() {
        date = moment(this.stamp).fromNow();
        return date;
    }
});

/*
 * Comment list template helpers
 */
Template.comments.helpers({
    comments: function() {
        postComments = Comments.find(
                {postId: this._id},
                {sort: {stamp: -1}}
            );
        // Loop them and pop off if user has flagged
        
        return postComments;
    },
});

/*
 * New comment triggers
 */
Template.newComment.events({

    /*
     * Contract container when clicking minus sign div
     */
    'click .new-comment-rollover-minus': function (e) {
        $(".new-comment-rollover-minus").slideUp("", function() {
            $(".new-comment-rollover-plus").show();
        });
        $(".new-comment-form").slideUp("slow");
    },
    /*
     * Expand container when clicking plus sign div
     */
    'click .new-comment-rollover-plus': function (e) {
        $(".new-comment-rollover-plus").hide();
        $(".new-comment-rollover-minus").slideDown("slow");
        $(".new-comment-form").slideDown("slow");
        $(".new-comment-form").children(".comment-fieldset").children(".comment-input").focus();
    },
    /*
     * Submit form in place when enter is pressed in input field
     */
    'keypress .comment-fieldset': function (e) {
        if (e.which === 13) {
            
            // Trigger login/signup box if not lgged in
            if (!Meteor.user()) {
                // TODO: Open login box
                errorThrow("Gotta be logged in ta comment, mannngggzzz");
                return false;
            }
            
            var comment = {
                postId: this._id,
                stamp: new Date().getTime(),
                username: Meteor.user().username,
                comment: e.target.value.trim(),
                anonymous: $('input[name=comment-anonymous]:checked').val(),
                haters: 0,
                flags: 0
            }

            // Add comment to database
            Meteor.call ('commentNew', comment, function (error) {
                if (error) {
                    errorThrow(error.reason);
                } else {
                    // Clear form values
                    $(".new-comment-form").find("input[type=checkbox], textarea").val("");
                }
            });

        } else if (e.which === 27) {
            /*
             * Close new comment form when Esc key is pressed
             *
             * TODO:
             *      Does not work, ever! >:O
            e.preventDefault();
            $(".new-comment-rollover-minus").slideUp("", function() {
                $(".new-comment-rollover-plus").show();
            });
            $(".new-comment-form").slideUp("slow");
            return false;
             */
        }
    },
    /*
     * Prevent submission
     *  
     */
    'submit': function(e) {
        e.preventDefault();
        return false;
    }

});

/*
 * Comment list triggers
 *
 * TODO:
 *  These should maybe be in just comment events
 */
Template.comment.events({
    /*
     * Show and hide comment flagger
     */
    'mouseenter .comment-container': function (event) {
        $(event.currentTarget).children(".comment-flag-holder").hide();
        $(event.currentTarget).children(".comment-flag").show();
    },
    'mouseleave .comment-container': function (event) {
        $(event.currentTarget).children(".comment-flag").hide();
        $(event.currentTarget).children(".comment-flag-holder").show();
    },
    /*
     * Flagging comment
     * Insert into hidden collection
     * Hidden for all or just for user
     * Need post id
     *
     */
    'click .comment-flag': function (e) {
        Meteor.call ('flagComment', this._id, function (error) {
            if (error) {
               errorThrow(error.reason);
            }
        });
    },
    /*
     * Hate on it
     */
    'click .hate-comment': function (e) {
        e.preventDefault();
        Meteor.call ('hateComment', this._id, function (error) {
            if (error) {
               errorThrow(error.reason);
            }
        });
    }
});
