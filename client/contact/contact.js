Template.contact.helpers ({

    promptName: function () {
        return Strings.contactName;
    },
    promptMail: function () {
        return Strings.contactMail;
    },
    promptMesg: function () {
        return Strings.contactMesg;
    }
});

Template.contact.events ({
    
    /**
     *  User interacting w/contact form inputs
     */
    'focus .form-control': function(e) {
        // Clear prompt on entry
        $(e.target).val("");
    },

    'click .form-control': function(e) {

        // Prevent bootstrap from closing contact form
        $(e.currentTarget).parent().parent().on('hide.bs.dropdown', function () {
            return false;
        });
        e.stopPropagation();
        
        return false;

    },

    /**
     * Insert contact data into log
     *
     * TODO:
     *      Only need message if logged in!
     */
    'keypress .form-control': function (e) {

        // Do nothing unless enter pressed
        if (e.which === 13) {

            // Populate stuff we need
            var id = e.currentTarget.id;
            var name = $('#contact-name').val().trim();
            var mail = $('#contact-mail').val().trim();
            var mesg = $('#contact-mesg').val().trim();
            var defaultName = Strings.contactName;
            var defaultMail = Strings.contactMail;
            var defaultMesg = Strings.contactMesg;
            var successMesg = Strings.contactRcpt;

            // Check everything
            switch (id) {
                case 'contact-name':
                    if (!name) {
                        errorThrow("eYe wantz ta knoo000 ur nammmee");
                        $('#contact-name').focus();
                        return false;
                    } else {
                        $('#' + id).next().focus();
                        return false;
                    }
                    break;
                case 'contact-mail':
                    if (!mail) {
                        errorThrow("gimme ur address i w0n't spamz uuuuu");
                        $('#contact-mail').focus();
                        return false;
                    } else {
                        $('#' + id).next().focus();
                        return false;
                    }
                    break;
                case 'contact-mesg':
                    if (!name) {
                        errorThrow("eYe wantz ta knoo000 ur nammmee");
                        $('#contact-name').focus();
                        return false;
                    }
                    if (!mail) {
                        errorThrow("gimme ur address i w0n't spamz uuuuu");
                        $('#contact-mail').focus();
                        return false;
                    }
                    if (!mesg) {
                        errorThrow("u got b33f w/m3 er wut");
                        $('#contact-mesg').focus();
                        return false;
                    }
                    break;
            }

            // They made it, format document
            contactId = ContactLog.insert({
                name: name,
                mail: mail,
                mesg: mesg,
                ip: Meteor.userIp,
                stamp: new Date().getTime()
            });

            if (!contactId) {
                // Failure
                errorThrow("we haz failured to l0g yo requestzzs");
                $('#contact-mesg').focus();
                return false;
            } else {
                // Success, clear values and close form
                $('#contact-name').val("");
                $('#contact-mail').val("");
                $('#contact-mesg').val(successMesg);
                $('#contact').parent().dropdown('toggle');
                return true;
            }

        // End keypress === 13
        }
       
   // End keypress
   }

// End events
});
