/*
 * Notifications template helpers
 */
Template.notifications.helpers({
    notifications: function() {
        var r = new Array();            // return object
        var o = Notifications.find({}, 
                {sort: {stamp: -1}});   // notification object
        var p = null;                   // single post
        var c = null;                   // single comment
        o.forEach(function (i) {        // i for item
            if (p = Posts.findOne({_id: i.objectId})) {
                // Temp needs to be reset each loop
                var t = new Array();
                // Tricking iron pathFor
                t._id = p._id;
                // Build notification object
                t.object = i.username
                                    + ' ' + i.action
                                    + ' your post';
                r.push(t);
            } else if (c = Comments.findOne({_id: i.objectId})) {
                // Temp needs to be reset each loop
                var t = new Array();
                // Tricking iron pathFor
                t._id = c.postId;
                // Build notification object
                t.object = i.username
                                    + ' ' + i.action
                                    + ' your comment';
                r.push(t);
            }
        });

        /*
         * Show loading bar
         */
        if (0 < r.length) {
            $('#iron-router-progress').hide();
        }
        return r;
    }
});

/*
 * Notification template triggers
 */
Template.notifications.events({
    
    'click a': function(e) {
        // Log it
        // Mark as read
    },

});
