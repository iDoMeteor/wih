Template.home.rendered = function() {
    /*
     * Replace accounts-ui text link w/icon
     */
    if(!Meteor.user()) {
        $('#login-sign-in-link').replaceWith('<a class="login-link-text" id="login-sign-in-link"><span title="Drop in" class="glyphicon glyphicon-log-in"></span></a>');
    } else {
        $('#login-name-link').replaceWith('<a class="login-link-text" id="login-name-link"><span title="GTFO" class="glyphicon glyphicon-log-out"></span></a>');
    }
};

Template.home.helpers ({
    /*
     * Determine if user has activity log
     */
    hasActivityLog: function() {
        var count = ActivityLog.find().count();
        if (0 < count) {
            return true;
        } else {
            return false;
        }
    },
    /*
     * Determine if user has favorites
     */
    hasFavorites: function() {
        var count = Favorites.find().count();
        if (0 < count) {
            return true;
        } else {
            return false;
        }
    },
    /*
     * Determine if user has notifications
     */
    hasNotifications: function() {
        var count = Notifications.find().count();
        if (0 < count) {
            return true;
        } else {
            return false;
        }
    },

});
