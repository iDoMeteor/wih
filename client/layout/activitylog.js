/*
 * Activity Log template helpers
 */
Template.activitylog.helpers({
    activitylog: function() {
        var r = new Array();            // return object
        var o = ActivityLog.find({}, 
                {sort: {stamp: -1}});   // activitylog object
        var p = null;                   // single post
        var c = null;                   // single comment
        o.forEach(function (i) {        // i for item
            /*
             * If object is not a comment or post
             * we ignore it for now, later might
             * make usernames or tags actionable.
             * Currently, non-page/comment objects
             * are ignored (r.push inside ifs).
             */
            if (p = Posts.findOne({_id: i.objectId})) {
                // Temp needs to be reset each loop
                var t = new Array();
                // Tricking iron pathFor
                t._id = p._id;
                // Building favorite object
                t.object = 'You '
                            + i.action
                if (Meteor.userId() == p.userId) {
                    // Action should be 'created' or some ish
                    t.object = t.object + ' a new post'
                } else {
                    // Action should be commented/hated/flag/fav
                    t.object = t.object 
                            + ' '
                            + p.username
                            + "'s post";
                }
                // Add to stack
                r.push(t);
            } else if (c = Comments.findOne({_id: i.objectId})) {
                // Comment activity
                var t = new Array();
                // Tricking iron pathFor
                t._id = c.postId;
                // Building activity object
                t.object = 'You '
                            + i.action
                            + ' '
                if (!!i.postId) {
                    // Commenting on a post
                    // But to whom does it belong?
                    p = Posts.findOne({_id: i.postId})
                    if (Meteor.userId() == p.userId) {
                        t.object = t.object + " your own post";
                    } else { 
                        t.object = t.object 
                                     + p.username
                                     + "'s post";
                    }
                } else {
                    // Hating/flagging someone else's comment
                    t.object = t.object 
                            + c.username
                            + "'s comment";
                }
                // Add to stack
                r.push(t);
            }
        });

        /*
         * Show loading bar
         */
        if (0 < r.length) {
            $('#iron-router-progress').hide();
        }
        return r;
    }
});
