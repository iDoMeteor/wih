/*
 * Favorites template helpers
 */
Template.favorites.helpers({
    favorites: function() {
        var r = new Array();        // return object
        var o = Favorites.find();   // favorites object
        var p = null;               // single post
        o.forEach(function (i) {
            if (p = Posts.findOne({_id: i.objectId})) {
                // Temp needs to be reset each loop
                var t = new Array();
                // Tricking iron pathFor
                t._id = p._id;
                // Building favorite object
                //
                // TODO:
                //  Why won't accessing Strings work?
                t.object = p.username
                                + " " + 'hates'
                                + " " + p.target
                                + " " + 'because'
                                + " " + p.reason;
                r.push(t);
            }
        });

        /*
         * Show loading bar
         */
        if (0 < r.length) {
            $('#iron-router-progress').hide();
        }
        return r;
    }
});

/*
 * Favorite template triggers
 */
Template.favorites.events({
    
    'click a': function(e) {
        // Log it
    },

});
