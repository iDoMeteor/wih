Meteor.startup(function () {

    // Set document title
    Meteor.autorun(function() {
        document.title = Strings.siteTitle;
    });

    // Assign unique IDs to all posts
    $(".post-container").each(function() {
        $(this).uniqueId();
    });

    // Assign unique IDs to all post flags
    $(".post-flag").each(function() {
        $(this).uniqueId();
    });

    // Assign unique IDs to comments
    $(".comment").each(function() {
        $(this).uniqueId();
    });

    // Assign unique IDs to new comment forms
    $(".new-comment-form").each(function() {
        $(this).uniqueId();
    });

    // Assign unique IDs to comment flags
    $(".comment-flag").each(function() {
        $(this).uniqueId();
    });

});
