# What I Hate
My first Meteor project ever, from early summer 2014 using Meteor 0.7.2.

The first half of 2014.. I was in quite a bad mood after a complicated break up.  Therefore, I made this to give others a place to vent.  The idea is you leave a tweet or short story for people to hate on.  Others can comment, hate on it, flag it, etc.  
I ended up having to make some $ and help a friend out w/a LAMP oriented business that needed fires put out like every day.

The hater version was just the MVP really, I had also bought domains for WhyMarquette, WhyMichigan and WhyUSA that were going to be used as positivity builders! ;D

This is just here for historical purposes.  It had some nice routing and other goodies going on, as well as having used Git from the start (as opposed to my current projects in which until recently I have been relying completely on vim).

# Y U No Finish!?

Aside from getting super busy saving http://prowebmarketing.com from themselves, a big upgrade in Meteor and Iron Router that made me lose interest.  All that I had left to do was execute the external API hooks for the social services (it was built to be completely integrated into them properly).

I may have been in a bad mood, but I always write clean, clear code! :)
