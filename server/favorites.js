/*
 * Favorite publications
 */
Meteor.publish('favorites', function() {
    var options = {
        userId: 0,
        sort: {stamp: -1}, 
        limit: Strings.favoriteLimit
    };
    
    return Favorites.find({userId: this.userId}, options);
});


/*
 * Permissions for Notifcation collection
Favorites.allow({
    insert: function () {
        // Might want to ignore non-logged in users
        // if (Meteor.user()) return true;
        return !! Meteor.user();
    },
    remove: function () {
        // Might want to ignore non-logged in users
        // This needs to check for ownership
        var result = (this.userId == Meteor.userId());
        return  result;
    },
});
 */
