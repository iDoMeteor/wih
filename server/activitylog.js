/*
 * Activity Log publications
 *
 */
Meteor.publish('activitylog', function() {

    // Set a reasonable limit
    // and publish the rest on an activity page
    var options = {
        userId: 0,
        sort: {stamp: -1}, 
        limit: Strings.activityLimit
    };
    
    return ActivityLog.find({userId: this.userId}, options);

});
