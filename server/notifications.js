/*
 * Notification publications
 *
 * Note that the notification is for user
 * identified by ownerId.  The userId in
 * this document refers to the username
 * which is the user who caused the action
 *
 */
Meteor.publish('notifications', function() {

    // Set a reasonable limit
    // and publish the rest on an activity page
    var options = {
        userId: 0,
        sort: {stamp: -1}, 
        limit: Strings.notificationLimit
    };
    
    return Notifications.find({ownerId: this.userId}, options);

});
