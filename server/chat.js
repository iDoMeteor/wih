/*
 * Chat publications
 */
Meteor.publish('chat', function() {

    // Set a reasonable limit
    var options = {
        userId:     0,
        ip:         0,
        sort:       {stamp: -1}, 
        limit:      Strings.chatLimit
    };
    
    return Chat.find({}, options).sort({stamp: 1});

});

/*
 * Permissions for Chat collection
 */
Chat.allow({
    insert: function () {
        // Must be logged in to flag
        return true;
    },
    update: function () {
       // Not used yet
       return false;
    },
    remove: function () {
       // Not used yet
       return isadmin();
    }
});

/*
 * Chat methods
 */
Meteor.methods ({

    /*
     * Insert chat line
     */
    chatAdd: function (s) {

        check (s, String);

        if (!s) {
            throw new Meteor.Error (
                422, "Chat line cannot be blank"
            );
        }

        // Populate chat object
        if (this.userId) {
            // Registered user
            var line = {
                line:       s,
                stamp:      new Date().getTime(),
                ip:         Meteor.userIp,
                username:   Meteor.user().username,
                userId:     this.userId
            };
        } else {
            // Anonymous user
            var line = {
                line:       s,
                stamp:      new Date().getTime(),
                ip:         Meteor.userIp,
                username:   'anon'
            };
        }

        var lineId = Chat.insert(line);
        
        if (lineId) {
            return lineId;
        }

    },

    /*
     * Remove chat line
     */
    chatRemove: function (id) {

        Chat.remove({objectId: id});

    },

});
