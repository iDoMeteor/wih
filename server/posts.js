/*
 * Post publications
 */
Meteor.publish('postsAll', function() {
    // TODO:
    // Are we still subscribing everyone to this?
    return Posts.find({}, {userId: 0});
});

Meteor.publish('postSingle', function(id) {
    // Should check user's specific flags in addition to site flagged
    return Posts.find({_id: id}, {userId: 0});
});

Meteor.publish('posts', function() {
    // Get flag IDs by IP
    // Get flag IDs by user
    // Get flag IDs by owner
    // Get flag IDs by admin
    // Get flag IDs by system limit
    // Combine all flag IDs
    // Remove them from the collection
    // Format options object
    options = {userId: 0};
    // Send'em out
    cursor = Posts.find({}, options);
    // Try cursor.remove?
    // Might need to reset it after foreaching
    return cursor;
});

Meteor.publish('postsByAuthor', function(author) {

    // Safety first
    check(author, String);
    // Return it
    return Posts.find({
            author: author,
            flagged: false,
        });
});

Meteor.publish('postsFieldSubset', function(value) {
    return Posts.find({userId: 0},
        {fields: {
            field: value,
            }
        });
});

/*
 * Permissions for Posts collection
 */
Posts.allow({
    insert: function () {
        // Only for logged in users
        // Failure should return error / encourage sign up
        return !! Meteor.user();
    },
    // User able to update the comment count by 1 when commenting
    update: function () {
        if (Meteor.user()) return true;
    },
    remove: false,
});

/*
 * Denials for Posts collection
 */
Posts.deny({
    // Deny when callback returns true
    // Limit update fields
    // This is block comment count updates no matter what i do :(
    /*
     *update: function (id, post, fieldNames) {
     *    // Not allowing owner to click own post
     *    if (isOwner (id, post)) {
     *        // editable fields list
     *        return (_.without(fieldNames, 'target', 'reason').length > 0);
     *    } else {
     *        // updating comments, called from server upon successful insert
     *        return post;
     *    }
     *}
     */
});

/*
 * Server side post functions
 */
Meteor.methods ({

    /*
     * New post submission
     *
     * Set local vars
     * Check login status
     * Check values from clients
     * Check dupe
     * Prevent extraneous values
     * Insert post
     * Return errors if necessary
     *
     * TODO:
     *  Dupes
     *  Disable submission for X time
     *  Allow editing of posts/comments for X seconds
     *  Racial slur filter
     */
    postNew: function (postValues) {

        // Should set default values then check against incoming
        // ie; post.serverFB, user.Twitter (for sent to fb, sent to tw)
        var dupeTarget = false;
        var dupeReason = false;
        // Make sure they don't cheat
        // postValues.user = Meteor.user();
        // comments = 0;
        // haters = 0;

        if (!Meteor.user()) {
            throw new Meteor.Error (
                401, "Nice try, hax0r.  Now login."
            );
        }

        if (!postValues.target) {
            throw new Meteor.Error (
                422, "You must enter a target for your hatred"
            );
        }

        if (!postValues.reason) {
            throw new Meteor.Error (
                422, "Your hatred must have justification"
            );
        }

        if (postValues.anonymous) {
            postValues.anonymous = true;
        }

        // Find dupes
        // throw 302 & go to post / use find/count

        // Add user id
        postValues.userId = this.userId;

        // Whitelisted values
        var post = _.extend(
               _.pick(
                   postValues,
                   'userId',
                   'target',
                   'reason',
                   'stamp',
                   'username',
                   'anonymous',
                   'comments',
                   'flags',
                   'haters'
                   )
               );
        // Insert it
        post._id = Posts.insert(post);

        // Log it
        if (post._id) {
            Meteor.call ('activitylogCreate',
                    post,
                    Strings.logPostNew
                    );
        }

        return post._id;

    },

    /*
     * Increment flag count
     */
    flagPost: function (postId) {

        // Should really check to see if post is valid
        if (!postId) {
            throw new Meteor.Error (
                422, "You must enter a target for your flaggery"
            );
        }

        // Grab object owner's id to create notification
        post = Posts.findOne({_id: postId});

        // Increment flag count
        if (this.userId == post.userId) {
            Posts.update (postId, {
                $set: {flagOwner: 1}
            });
        } else {
            Posts.update (postId, {
                $inc: {flags: 1}
            });
        }

        // Log it in the flag log
        if (this.userId) {
            FlagLog.insert({
                objectId: postId,
                userId: this.userId,
                ip: Meteor.userIp,
                stamp: new Date().getTime(),
            });
        } else {
            FlagLog.insert({
                objectId: postId,
                ip: Meteor.userIp,
                stamp: new Date().getTime(),
            });
        }
        
        // Log it in activity log
        Meteor.call ('activitylogCreate',
                post,
                Strings.logPostFlag
                );

        // Call notification
        Meteor.call ('notificationCreate', 
                post, 
                Strings.postFlagged, 
                false, 
                function (error) {
                    if (error) {
                        // Notification failure does throw errors atm
                        // errorThrow(error.reason);
                    } 
                }
        );


    },

    /*
     * Increment hate count
     */
    hatePost: function (postId) {

        /* Uncomment to disallow anonymous haters
            // Check for login
            if (!this.userId) {
                throw new Meteor.Error (
                    401, "Nice try, hax0r.  Now login."
                );
            }
        */

        // Should really check to see if post is valid
        if (!postId) {
            throw new Meteor.Error (
                422, "You must enter a target for your hatred"
            );
        }

        //  Check hate log for previous vote from this user/ip
        if (this.userId) {
            hated = HateLog.findOne({
                objectId: postId,
                userId: this.userId,
            });
            if (!!hated) {
                hated.stamp = moment(hated.stamp).fromNow();
                throw new Meteor.Error (
                    422, "You already hated on this " + hated.stamp
                );
            }
        } else {
            hated = HateLog.findOne({
                objectId: postId,
                ip: Meteor.userIp,
            });
            if (!!hated) {
                hated.stamp = moment(hated.stamp).fromNow();
                throw new Meteor.Error (
                    422, "An anonymous user with your IP already hated on this " + hated.stamp
                );
            }
        }

        // Grab object owner's id to create notification
        post = Posts.findOne({_id: postId});

        // Do not let user hate their own comment
        if (this.userId == post.userId) {
            throw new Meteor.Error (
                401, "Don't hate on ur own shiz, homey"
            );
        }

        // Increment hate count
        Posts.update (postId, {
            $inc: {haters: 1}
        });

        // TODO:
        //  Should only do this on success
        //  Need to make a whole thing to handle this in general..
        //  http://docs.mongodb.org/manual/reference/method/db.collection.update/#writeresults-update
        
        // Log it in the hate log
        if (this.userId) {
            HateLog.insert({
                objectId: postId,
                userId: this.userId,
                ip: Meteor.userIp,
                stamp: new Date().getTime(),
            });
        } else {
            HateLog.insert({
                objectId: postId,
                ip: Meteor.userIp,
                stamp: new Date().getTime(),
            });
        }
        
        // Log it to site log
        Meteor.call ('activitylogCreate',
                post,
                Strings.logPostHate,
                'post');

        // Call notification
        Meteor.call ('notificationCreate', 
                post, 
                Strings.postHated, 
                false, 
                function (error) {
                    if (error) {
                        // Notification failure does throw errors atm
                        // errorThrow(error.reason);
                    } 
                }
        );


    }

});
