/*
 * Comment publications
 */
Meteor.publish('comments', function(id) {
    
    // Only comments for posts appearing on page should be published
    // return Comments.find({}, {limit: 10});
    if (id) {
        return Comments.find({_id: id});
    } else {
        return Comments.find();
    }

});


/*
 * Permissions for Comments
 */
Comments.allow({
    insert: function () {
        // Only for logged in users
        return !! Meteor.user();
    },
    // Use underscore to add a test for non logged in user voting (allow)
    update: requireLogin,
    remove: false,

});

/*
 * Denials for Comments
 */
Comments.deny({
    // Deny when callback returns true
    // Limit update fields
    update: function (userId, post, fieldNames) {
        // editable fields list
        return (_.without(fieldNames, 'comment').length > 0);
    },
});

/*
 * Server side comment functions
 */
Meteor.methods ({
    
    /*
     * New comment submission
     *
     * Set local vars
     * Check login status
     * Check values from clients
     * Check dupe
     * Prevent extraneous values
     * Insert comment
     * Return errors if necessary
     *
     * TODO:
     *  Dupes
     *  Disable submission for X time (short!)
     *  Allow editing of posts/comments for X seconds
     *  Associate post id properly
     */
    commentNew: function (commentValues) {

        /*
        var dupe = false;
        var stamp = new Date().getTime();
        var username = Meteor.user().username;
        var postId = null;
        var comment = null;
        var anonymous = false;
        var haters = 0;
        var flags = 0;
        */

        if (!Meteor.user()) {
            throw new Meteor.Error (
                401, "Aren't j00 slick b1tzy, try loggin' in"
            );
        }

        if (!commentValues.comment) {
            throw new Meteor.Error (
                422, "Comment cannot be blank"
            );
        }

        // Select only values we're sure we want
        var comment = _.extend(
               _.pick(
                   commentValues,
                   'postId',
                   'stamp',
                   'username',
                   'comment',
                   'anonymous',
                   'haters',
                   'flags'
                   )
               );
        
        // Add user id
        comment.userId = this.userId;

        // Insert
        comment._id = Comments.insert(comment);
        
        // Upon success, set timer & create notification
        if (comment._id) {
            
            // TODO:
            //  Set time until user can post again
            
            // Increment comment count
            Posts.update (comment.postId, {
                $inc: {comments: 1}
            });

            // Grab object owner's id to create notification & log
            post = Posts.findOne({_id: comment.postId});

            // Log it
            Meteor.call ('activitylogCreate',
                    comment,
                    Strings.logCommentNew
                    );

            // Call notification
            Meteor.call ('notificationCreate', 
                    post, 
                    Strings.commentAdded, 
                    false, 
                    function (error) {
                        if (error) {
                            // Notification failure does throw errors atm
                            // errorThrow(error.reason);
                        } 
                    }
            );

            return comment._id;

        } else {
            return false;
        }

    },

    /*
     * Comment is being flagged
     */
    flagComment: function (commentId) {

        /* Allow non-logged in users..
            // Check for login
            if (!Meteor.user()) {
                throw new Meteor.Error (
                    401, "Nice try, hax0r.  Now login."
                );
            }
        */
        // Should really check to see if post is valid
        if (!commentId) {
            throw new Meteor.Error (
                422, "Can't flag something which does not exist"
            );
        }
        
        // Grab comment object for logging
        comment = Comments.findOne({_id: commentId});

        // Increment flag count
        if (this.userId == comment.userId) {
            Posts.update (commentId, {
                $set: {flagOwner: 1}
            });
        } else {
            Comments.update (commentId, {
                $inc: {flags: 1}
            });
        }

        // TODO:
        //  Should only do these on success
        
        // Log it in the flag log
        if (this.userId) {
            FlagLog.insert({
                objectId: commentId,
                userId: this.userId,
                ip: Meteor.userIp,
                stamp: new Date().getTime(),
            });
        } else {
            FlagLog.insert({
                objectId: commentId,
                ip: Meteor.userIp,
                stamp: new Date().getTime(),
            });
        }
        
        
        // Log it
        Meteor.call ('activitylogCreate',
                comment,
                Strings.logCommentFlag
                );

        // Call notification
        Meteor.call ('notificationCreate', 
                comment, 
                Strings.commentFlagged,
                false, 
                function (error) {
                    if (error) {
                        // Notification failure does throw errors atm
                        // errorThrow(error.reason);
                    } 
                }
        );

    },

    /*
     * Increment hate count if user is logged in
     */
    hateComment: function (commentId) {

        /* Allow non-logged in users..
            // Check for login
            if (!Meteor.user()) {
                throw new Meteor.Error (
                    401, "Nice try, hax0r.  Now login."
                );
            }
        */
        // Should really check to see if post is valid
        if (!commentId) {
            throw new Meteor.Error (
                422, "You must enter a target for your hatred"
            );
        }
        
        //  Check hate log for previous vote from this user/ip
        if (this.userId) {
            hated = HateLog.findOne({
                objectId: commentId,
                userId: this.userId,
            });
            if (!!hated) {
                hated.stamp = moment(hated.stamp).fromNow();
                throw new Meteor.Error (
                    422, "You already hated on this " + hated.stamp
                );
            }
        } else {
            hated = HateLog.findOne({
                objectId: commentId,
                ip: Meteor.userIp,
            });
            if (!!hated) {
                hated.stamp = moment(hated.stamp).fromNow();
                throw new Meteor.Error (
                    422, "An anonymous user with your IP already hated on this " + hated.stamp
                );
            }
        }

        // Grab object owner's id to create notification
        comment = Comments.findOne({_id: commentId});

        // Do not let user hate their own comment
        if (this.userId == comment.userId) {
            throw new Meteor.Error (
                401, "Don't hate on ur own shiz, homey"
            );
        }

        // Increment hate count
        Comments.update (commentId, {
            $inc: {haters: 1}
        });

        // TODO:
        //  Should only do these on success
        
        // I think this hack will make it
        // easier to determine what's happening
        // for the activity log
        // Seems to be working, dunno why right now lol
        // Removing postId since following do not need
        delete comment.postId;

        // Log it in the hate log
        if (this.userId) {
            HateLog.insert({
                objectId: commentId,
                userId: this.userId,
                ip: Meteor.userIp,
                stamp: new Date().getTime(),
            });
        } else {
            HateLog.insert({
                objectId: commentId,
                ip: Meteor.userIp,
                stamp: new Date().getTime(),
            });
        }
        
        // Log it in activity log
        Meteor.call ('activitylogCreate',
                comment,
                Strings.logCommentHate
                );

        // Call notification
        Meteor.call ('notificationCreate', 
                comment, 
                Strings.commentHated, 
                false, 
                function (error) {
                    if (error) {
                        // Notification failure does throw errors atm
                        // errorThrow(error.reason);
                    } 
                }
        );

    }

});
