/*
 * Flag publications
 */
Meteor.publish('flags', function() {
    return Flags.find({userId: this.userId});
});


/*
 * Permissions for Notifcation collection
Flags.allow({
    insert: function () {
        // Might want to ignore non-logged in users
        // if (Meteor.user()) return true;
        return !! Meteor.user();
    },
    remove: function () {
        // Might want to ignore non-logged in users
        // This needs to check for ownership
        var result = (this.userId == Meteor.userId());
        return  result;
    },
});

 */
